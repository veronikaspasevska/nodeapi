var express = require("express");
var app = express();
app.set('port', process.env.PORT || 8000);
 
app.get("/", function(req, res) {
	res.json({
		res: "Try with the same HTTP method with /get endpoint" 
	});
});
 
app.get("/get", function(req, res) {
	res.json({ 
		req: "This was GET request.", 
		suggest: "Please try other CRUD APIs."});
});
 
app.post("/post", function(req, res) {
	res.send("You want to POST something?\n");
});
 
app.put("/put", function(req, res) {
	res.send("OK, the change is made.\n");
});
 
app.delete("/delete", function(req, res) {
	res.send("Sorry for DELETE request.\n");
});
 
app.listen(app.get('port'));
console.log(`Server is listening at port ${app.get('port')}.`);